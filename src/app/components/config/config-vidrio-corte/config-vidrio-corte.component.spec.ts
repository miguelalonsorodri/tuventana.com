import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigVidrioCorteComponent } from './config-vidrio-corte.component';

describe('ConfigVidrioCorteComponent', () => {
  let component: ConfigVidrioCorteComponent;
  let fixture: ComponentFixture<ConfigVidrioCorteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigVidrioCorteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigVidrioCorteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
