import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigVidrioPrecioComponent } from './config-vidrio-precio.component';

describe('ConfigVidrioPrecioComponent', () => {
  let component: ConfigVidrioPrecioComponent;
  let fixture: ComponentFixture<ConfigVidrioPrecioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigVidrioPrecioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigVidrioPrecioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
