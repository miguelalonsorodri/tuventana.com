import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigOtrosPrecioComponent } from './config-otros-precio.component';

describe('ConfigOtrosPrecioComponent', () => {
  let component: ConfigOtrosPrecioComponent;
  let fixture: ComponentFixture<ConfigOtrosPrecioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigOtrosPrecioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigOtrosPrecioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
