import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigPerfilesPrecioComponent } from './config-perfiles-precio.component';

describe('ConfigPerfilesPrecioComponent', () => {
  let component: ConfigPerfilesPrecioComponent;
  let fixture: ComponentFixture<ConfigPerfilesPrecioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigPerfilesPrecioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigPerfilesPrecioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
