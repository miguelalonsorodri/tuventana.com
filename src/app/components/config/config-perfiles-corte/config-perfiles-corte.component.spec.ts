import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigPerfilesCorteComponent } from './config-perfiles-corte.component';

describe('ConfigPerfilesCorteComponent', () => {
  let component: ConfigPerfilesCorteComponent;
  let fixture: ComponentFixture<ConfigPerfilesCorteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigPerfilesCorteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigPerfilesCorteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
