import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ToolbarComponent } from './components/shared/toolbar/toolbar.component';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { ConfiguradorComponent } from './components/configurador/configurador.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { PresupuestosComponent } from './components/presupuestos/presupuestos.component';
import { ConfigPerfilesCorteComponent } from './components/config/config-perfiles-corte/config-perfiles-corte.component';
import { ConfigPerfilesPrecioComponent } from './components/config/config-perfiles-precio/config-perfiles-precio.component';
import { ConfigVidrioPrecioComponent } from './components/config/config-vidrio-precio/config-vidrio-precio.component';
import { ConfigVidrioCorteComponent } from './components/config/config-vidrio-corte/config-vidrio-corte.component';
import { ConfigOtrosPrecioComponent } from './components/config/config-otros-precio/config-otros-precio.component';
import { CampañasComponent } from './components/campañas/campañas.component';
import { NewuserComponent } from './components/newuser/newuser.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { PresupuestoComponent } from './components/presupuesto/presupuesto.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    HomeAdminComponent,
    ConfiguradorComponent,
    FooterComponent,
    PresupuestosComponent,
    ConfigPerfilesCorteComponent,
    ConfigPerfilesPrecioComponent,
    ConfigVidrioPrecioComponent,
    ConfigVidrioCorteComponent,
    ConfigOtrosPrecioComponent,
    CampañasComponent,
    NewuserComponent,
    ClientesComponent,
    PresupuestoComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
